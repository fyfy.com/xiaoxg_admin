import Vue from 'vue';
import Router from 'vue-router';
//定懒加载方式的路由
const Login   = resolve => require(['@/components/auth/login.vue'], resolve);
const Base    = resolve => require(['@/components/layout/base.vue'], resolve);
//主页
const Main    = resolve => require(['@/components/index/index.vue'], resolve);
//分类
const Cate    = resolve => require(['@/components/cate/index.vue'], resolve);
//用户
const User    = resolve => require(['@/components/user/index.vue'], resolve);
//模板消息
const Tmplmsg = resolve => require(['@/components/tmplmsg/index.vue'], resolve);
//管理人员
const Manage  = resolve => require(['@/components/user/manage.vue'], resolve);
//FormID
const FormID  = resolve => require(['@/components/formid/index.vue'], resolve);
//统计分析
const VideoCount  = resolve => require(['@/components/videocount/index.vue'], resolve);
//配置项
const Config  = resolve => require(['@/components/config/index.vue'], resolve);
//按钮分析
// const Count  = resolve => require(['@/components/count/index.vue'], resolve);
//404
const Error404= resolve => require(['@/components/page/404.vue'], resolve);

Vue.use(Router);
export const constantRouterMap= [
    {path: '/', name: 'Login', component: Login, meta: {title: '登录', login:true}},
    {
        path: '/',
        component: Base,
        children:[
            {
                path: '/error/404',
                meta: {title:'404',show:false},
                component: Error404,
            },
        ],
    }
];
//异步挂载的路由
export const asyncRouterMap = [
    {
        path: '/',
        component: Base,
        children:[
            {
                path: '/cate',
                name: 'Cate',
                meta: {title:'分类管理',show:true,'icon':'fa fa-align-left'},
                component: Cate,
            },
            {
                path: '/main',
                name: 'Main',
                meta: {title:'视频管理',show:true,'icon':'fa fa-file-video-o'},
                component: Main,
            },
            {
                path: '/videocount',
                name: 'VideoCount',
                meta: {title:'统计分析',show:true,'icon':'fa fa-pie-chart'},
                component: VideoCount,
            },
            {
                path: '/config',
                name: 'Config',
                meta: {title:'配置管理',show:true,'icon':'fa fa-wrench'},
                component: Config,
            },
            {
                path: '/formid',
                name: 'FormID',
                meta: {title:'FormID',show:true,'icon':'fa fa-pie-chart'},
                component: FormID,
            },
            {
                path: '/tmplmsg',
                name: 'Tmplmsg',
                meta: {title:'模板消息',show:true,'icon':'fa fa-comments'},
                component: Tmplmsg,
            },
            {
                path: '/user',
                name: 'User',
                meta: {title:'用户管理',show:true,'icon':'fa fa-user-o'},
                component: User,
            },
            {
                path: '/manage',
                name: 'Manage',
                meta: {title:'管理人员',show:true,'icon':'fa fa-user-circle-o'},
                component: Manage,
            },
        ],
    },
    { path: '*',name:'404', redirect: '/error/404',meta: {show:false}, }
];
//实例化vue的时候只挂载constantRouter
export default new Router({
    mode: 'history',
    routes: constantRouterMap
});
