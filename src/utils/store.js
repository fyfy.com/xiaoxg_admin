import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import storage from "./storage";
import { asyncRouterMap, constantRouterMap } from '../router';
import { UserInfo,UserLogout} from '../utils/axios';
const store = new Vuex.Store({
    state: {
        routers: [],//权限筛选后的路由
        active: '',//菜单选中
        mainHeight: document.documentElement.clientHeight-160,//正文内容高度
        loading: false,//加载效果
        slider: false,//菜单开关
        upload: '/manage/upload',//上传图片地址
        tokenInfo:null,//登陆token信息
        user: null, //用户信息
    },
    getters: {
        routers:(state)=> {return state.routers;},
        active:(state)=> {return state.active;},
        leftMenuHeight:(state)=> {return state.leftMenuHeight;},
        leftMenuHeightNot:(state)=> {return state.leftMenuHeightNot;},
        mainHeight:(state)=> {return state.mainHeight;},
        loading:(state)=> {return state.loading;},
        upload:(state)=> {return state.upload;},
        slider:(state)=> {return state.slider;},
        tokenInfo:(state)=> {return state.tokenInfo;},
        refresh:(state)=> {return state.refresh;},
        user:(state)=>{return state.user;},
    },
    mutations: {
        SET_ROUTERS(state,param){state.routers = param;},
        SET_ACTIVE(state,param){state.active = param;},
        SET_LOADING(state,param){state.loading = param;},
        SET_USER(state,param){state.user = param;},
        SET_SLIDER(state,param){state.slider = param;},
        USER_LOGIN(state,param){storage.set('token',param);},
        USER_LOGOUT(){storage.remove('token');},
    },
    actions: {
        //设置菜单开关
        setSlider({commit},param){
            commit('SET_SLIDER',param);
        },
        //设置登陆用户信息
        // setUser({commit},param){
        //     commit('SET_USER',param);
        // },
        //退出登录
        userLogout({commit}){
            return new Promise((resolve, reject) => {
                UserLogout().then((response) => {
                    commit('USER_LOGOUT');
                    commit('SET_USER',null);
                    resolve(response);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        //登录
        userLogin({commit},param){
            return new Promise((resolve) => {
                commit('USER_LOGIN',param);
                resolve();
            });
        },
        //登录用户信息
        userInfo(){
            return new Promise((resolve, reject) => {
                UserInfo().then((response) => {
                    resolve(response.data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        //开启加载效果
        setLoading({commit},param){
            commit('SET_LOADING',param);
        },
        //设置菜单选中
        setActive({commit},param){
            commit('SET_ACTIVE',param);
        },
        //权限过滤
        generateRoutes({commit},param){
            //全部执行完再返回
            return new Promise(resolve => {
                const user = param;//用户信息
                const accessedRouters = asyncRouterMap.filter(v => {
                    return true;
                });
                commit('SET_USER', user);
                commit('SET_ROUTERS', accessedRouters);
                resolve();
            });
        },
    }
});
export default store;
