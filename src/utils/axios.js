import axios from 'axios';
import qs from 'qs';
// let base = 'https://www.adrenmai.com/api';
let base = '/manage';
//别名命名规则：模块+操作，常用操作：list(列表),info(详情),update(更新),destroy(删除),active(激活/启用),forbid(锁定/禁用)
//组件命名规则：一个导航一个目录，导航下的子菜单一个菜单一个文件
//系统登陆
export const UserLogin = params => { return axios.post(`${base}/login`, qs.stringify(params)).then(res => res.data); };//登录
export const UserLogout = params => { return axios.get(`${base}/logout`, params).then(res => res.data); };//退出登录
export const Captcha = params => { return axios.get(`${base}/captcha`, params).then(res => res.data); };//验证码
//用户管理部分
export const UserList = params => { return axios.get(`${base}/user`, params).then(res => res.data); };//用户列表
export const UserInfo = params => { return axios.get(`${base}/info`, params).then(res => res.data); };//用户基本信息
//管理人员
export const Manage = params => { return axios.get(`${base}/admin`, params).then(res => res.data); };//管理人员
export const ManageList = params => { return axios.get(`${base}/admin/list`, params).then(res => res.data); };//管理人员
export const ManageDestory = params => { return axios.delete(`${base}/admin/destroy`, params).then(res => res.data); };
export const ManageUpdate = params => { return axios.post(`${base}/admin/update`, params).then(res => res.data); };
//视频分类
export const CateList = params => { return axios.get(`${base}/cate`, params).then(res => res.data); };//视频分类列表
export const CateDestory = params => { return axios.delete(`${base}/cate/destroy`, params).then(res => res.data); };//视频分类删除
export const CateUpdate = params => { return axios.post(`${base}/cate/update`, qs.stringify(params)).then(res => res.data); };//视频分类更新
export const CateAll = params => { return axios.get(`${base}/cate/all`, params).then(res => res.data); };//所有分类
//视频
export const VideoList = params => { return axios.get(`${base}/video`, params).then(res => res.data); };//视频列表
export const VideoDestory = params => { return axios.delete(`${base}/video/destroy`, params).then(res => res.data); };//视频删除
export const VideoUpdate = params => { return axios.post(`${base}/video/update`, qs.stringify(params)).then(res => res.data); };//视频更新
//export const VideoTodayList = params => { return axios.get(`${base}/video/today`, params).then(res => res.data); };//视频列表
export const VideoDestoryAll = params => { return axios.delete(`${base}/video/destroy_all`, params).then(res => res.data); };//视频批量删除
//视频统计
export const VideoCountList = params => { return axios.get(`${base}/video/count`, params).then(res => res.data); };

//评论
export const CommentList = params => { return axios.get(`${base}/comment`, params).then(res => res.data); };//视频批量删除
export const CommentDestory = params => { return axios.delete(`${base}/comment/destroy`, params).then(res => res.data); };//视频删除
export const CommentDestoryAll = params => { return axios.delete(`${base}/comment/destroy_all`, params).then(res => res.data); };//视频批量删除
//我的模板
export const TmplmsgList = params => { return axios.get(`${base}/tmplmsg`, params).then(res => res.data); };
export const TmplmsgUpdate = params => { return axios.post(`${base}/tmplmsg/update`, params).then(res => res.data); };
export const TmplmsgPush = params => { return axios.post(`${base}/tmplmsg/push`, params).then(res => res.data); };
export const reToken = params => { return axios.post(`${base}/tmplmsg/token`, params).then(res => res.data); };  //重置token
//客服消息
export const CustomList = params => { return axios.get(`${base}/custom`, params).then(res => res.data); };
//数据分析
export const AnalyList = params => { return axios.get(`${base}/analy`, params).then(res => res.data); };
export const FormIDList = params => { return axios.get(`${base}/formid`, params).then(res => res.data); };

export const CountLogList = params => { return axios.get(`${base}/countlog`, params).then(res => res.data); };
export const CountCate = params => { return axios.get(`${base}/count`, params).then(res => res.data); };
//商场管理
export const MallList = params => { return axios.get(`${base}/mall`, params).then(res => res.data); }; //商品列表
export const MallUpdate = params => { return axios.post(`${base}/mall/update`, params).then(res => res.data); }; //商品信息更新
export const MallDestory = params => { return axios.delete(`${base}/mall/destroy`, params).then(res => res.data); }; //商品删除
export const MallDestoryAll = params => { return axios.delete(`${base}/mall/destroy_all`, params).then(res => res.data); }; //商品批量删除
//订单管理
export const OrderList = params => { return axios.get(`${base}/order`, params).then(res => res.data); }; //订单列表
export const OrderUpdate = params => { return axios.post(`${base}/order/update`, params).then(res => res.data); }; //订单信息更新
//配置信息
export const ConfigALL = params => { return axios.get(`${base}/config`, params).then(res => res.data); }; //获取all配置信息
export const ConfigMsg = params => { return axios.get(`${base}/config/msg`, params).then(res => res.data); }; //获取all配置信息
export const ConfigUpdate = params => { return axios.post(`${base}/config/update`, params).then(res => res.data); }; //更新配置信息
